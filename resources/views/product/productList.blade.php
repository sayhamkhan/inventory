@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Product List</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($productNames as $productName)
                                <tr class="">
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        {{$productName->product_name}}
                                    </td>
                                    <td>
                                        {{$productName->status}}
                                    </td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <a class="btn btn-success"
                                           href="{{route('updateProduct',$productName->id)}}"><i
                                                    class="fa fa-edit"></i></a>
                                        @if($productName->status=="active")
                                        <a href="{{route('inactiveBrand', $productName->id)}}" class="btn btn-info">
                                            Inactive
                                        </a>
                                        @else
                                            <a href="{{route('activeBrand', $productName->id)}}" class="btn btn-info">
                                                Active
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

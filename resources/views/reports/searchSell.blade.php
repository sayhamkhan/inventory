@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Search Sell Report</h1>
                        <form class="forms-sample" action="{{route('searchReportProcess')}}" role="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="firstDate">Start Date</label>
                                <br>
                                <input type="date" id="firstDate" class="form-control" name="firstDate">
                            </div>
                            <div class="form-group">
                                <label for="lastDate">End Date</label>
                                 <br>
                                <input type="date" id="lastDate" class="form-control" name="lastDate">
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Search</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

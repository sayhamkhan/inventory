@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Sell List</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Shop Name
                                    </th>
                                    <th>
                                        Brand Name
                                    </th>
                                    <th>
                                        Category Name
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Qty
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Total Price
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($SellData as $data)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$data->sellCustomar->cus_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->productBrand->brand_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->product_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->productCate->cate_name}}
                                        </td>
                                        <td>
                                            {{$data->qty}}
                                        </td>
                                        <td>
                                            {{$data->price}}
                                        </td>
                                        <td>
                                            {{$data->total_price}}
                                        </td>
                                        <td>
                                            @if($data->confirm==1)
                                                <a class="fontawesom_icon" href="{{route('confirmSellProcess', $data->id)}}">
                                                    <i class="fas fa-check-circle"></i>
                                                </a>
                                                <a class="fontawesom_icon" href="{{route('confirmSellProcess', $data->id)}}">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a class="fontawesom_icon fontawesom_icon_remove" href="{{route('confirmSellProcess', $data->id)}}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            @else
                                                <a class="fontawesom_icon fontawesom_icon_complete" href="#">
                                                    <i class="fas fa-check-circle"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

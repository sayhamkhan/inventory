@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Customar List</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Customar Name
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Due
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($activeCusData as $cusData)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$cusData->cus_name}}
                                        </td>
                                        <td>
                                            {{$cusData->phone}}
                                        </td>
                                        <td>
                                            {{$cusData->due}}
                                        </td>
                                        <td>
                                            {{$cusData->status}}
                                        </td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                Edit
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title edit_modal_title" id="exampleModalLabel">Edit Customar</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="" action="{{route('editCustomerProcess', $cusData->id)}}" role="" method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="brandName">Customar Name</label>
                                                                    <input type="text" name="brandName" class="form-control" id="brandName" value="{{$cusData->id}}">
                                                                </div>
                                                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                                                <button class="btn btn-danger" type="reset">Reset</button>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($cusData->status=="active")
                                                <a href="{{route('inactiveCustomer', $cusData->id)}}" class="btn btn-info">
                                                    Inactive
                                                </a>
                                            @else
                                                <a href="{{route('activeCustomer', $cusData->id)}}" class="btn btn-info">
                                                    Active
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

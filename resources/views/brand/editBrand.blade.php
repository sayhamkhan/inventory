@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')



        <div class="row">

            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h1 class="card-title">Edit Brand</h1>
                                <form class="forms-sample" action="{{route('editBrandProcess', $brand_info->id)}}" method="POST"
                                      role="form">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label for="brandName">Brand Name</label>
                                        <input type="text" name="brand_name" class="form-control"
                                               value="{{$brand_info->brand_name}}" id="brandName"
                                               placeholder="Enter Brand Name">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesTable extends Migration {

	public function up()
	{
		Schema::create('sales', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cus_id')->nullable();
			$table->integer('brand_id')->nullable();
			$table->integer('cate_id')->nullable();
			$table->integer('prod_id')->nullable();
			$table->integer('qty')->nullable();
			$table->integer('price')->nullable();
			$table->integer('total_price')->nullable();
			$table->integer('confirm')->default('1');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sales');
	}
}

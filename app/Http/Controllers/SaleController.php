<?php

namespace App\Http\Controllers;

use App\Models\ProductHistory;
use App\Models\Sale;
use Illuminate\Http\Request;
use Validator;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Customar;
use App\Models\Product;

class SaleController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */

  public function addSell()
  {
      $brandName=Brand::all();
      $catName=Category::all();
      $cusName=Customar::all();
      //dd($brandName);
      return view('sell.addSell', compact('brandName', 'cusName', 'catName'));
  }

  public function confirmSell()
  {
      $SellData=Sale::with('sellProduct')->with('sellProduct.productCate')->with('sellProduct.productBrand')->with('sellCustomar')->orderByRaw('FIELD(confirm, "no", "yes")')->orderBy('id', 'desc')->get();
      //dd($SellData);
      return view('sell.confirmSell', compact('SellData'));
  }

  public function jsonAddSell(Request $request){
    $data=Category::select('brand_id','id', 'cate_name')->where('brand_id',$request->id)->take(100)->get();
    return response()->json($data);
  }

  public function addSellProduct(Request $request){
    $cateData=Category::select('brand_id')->where('id',$request->id)->first();
    $data=Product::where('cate_id',$request->id)->where('brand_id',$cateData->brand_id)->take(100)->get();
    return response()->json($data);
  }

  public function addSellPrice(Request $request){
    $data=ProductHistory::where('product_name',$request->id)->where('qty', '>', 0)->take(100)->first();
    if(!empty($data)){
      return response()->json($data);
    }else{
      $data=Product::where('id',$request->id)->take(100)->first();
      return response()->json($data);
    }

  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function addSellProcess(Request $request)
  {
    //dd($request->all());
    //$data=ProductHistory::select('qty')->where('product_name',$request->productName)->where('qty', '>', 0)->take(100)->first();
    //dd($data);
    $inputs = $request->except('_token');

    $validator = Validator::make($inputs, [

        'customerName' => 'required',
        'brandName' => 'required',
        'cateName' => 'required',
        'productName' => 'required',
        'price' => 'required',
        'qty' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    //$test=$request->cateName;
    $checkProduct=Product::where('cate_id', $request->cateName)->where('brand_id', $request->brandName)->where('id', $request->productName)->where('price', $request->price)->first();
    //dd($checkProduct);
    if(empty($checkProduct)){

      $checkOldQty=ProductHistory::select('qty')->where('cate_id', $request->cateName)->where('brand_id', $request->brandName)->where('product_name', $request->productName)->where('qty', '>=', $request->qty)->first();
      $oldQty=ProductHistory::where('cate_id', $request->cateName)->where('brand_id', $request->brandName)->where('product_name', $request->productName)->first();
      //dd($oldQty);
      if (empty($checkOldQty)){
        session()->flash('message', 'You have only ' .$oldQty->qty. ' product. Please add sell again');
        return redirect()->back();
      }else{
        $oldProductSell=Sale::create([
            'cus_id'=>$request->input('customerName'),
            'brand_id'=>$request->input('brandName'),
            'cate_id'=>$request->input('cateName'),
            'prod_id'=>$request->input('productName'),
            'price'=>$request->input('price'),
            'qty'=>$request->input('qty'),
            'total_price'=>$request->price * $request->qty,
        ]);
        //$checkOldProduct=ProductHistory::where('cate_id', $request->cateName)->where('brand_id', $request->brandName)->where('product_name', $request->productName)->where('qty', '>=', $request->qty)->first();
        //
        //$updateOldProduct=ProductHistory::where('id', $checkOldProduct->id)->Update([
        //    'qty'=>$checkOldProduct->qty - $request->input('qty'),
        //]);
        session()->flash('message', 'Sell Created Successfully');
        return redirect()->back();
      }
    }else{
      $newProductSell=Sale::create([
          'cus_id'=>$request->input('customerName'),
          'brand_id'=>$request->input('brandName'),
          'cate_id'=>$request->input('cateName'),
          'prod_id'=>$request->input('productName'),
          'price'=>$request->input('price'),
          'qty'=>$request->input('qty'),
          'total_price'=>$request->price * $request->qty,
      ]);
      //$test=$request->productName;
      //dd($test);
      //$checkNewProduct=Product::where('cate_id', $request->cateName)->where('brand_id', $request->brandName)->where('id', $request->productName)->where('qty', '>=', $request->qty)->first();
      ////dd($checkNewProduct);
      //$updateNewProduct=Product::where('id', $checkNewProduct->id)->Update([
      //    'qty'=>$checkNewProduct->qty - $request->input('qty'),
      //]);
      session()->flash('message', 'Sell Created Successfully');
      return redirect()->back();
    }
  }
  public function confirmSellProcess($id)
  {
    $checkSell=Sale::where('id', $id)->first();
    $checkProduct=Product::where('cate_id', $checkSell->cate_id)->where('brand_id', $checkSell->brand_id)->where('id', $checkSell->prod_id)->where('price', $checkSell->price)->first();
    //dd($checkProduct);
    if(empty($checkProduct)){
      $oldProduct=ProductHistory::where('cate_id', $checkSell->cate_id)->where('brand_id', $checkSell->brand_id)->where('product_name', $checkSell->prod_id)->first();
        $updateOldProduct=ProductHistory::where('id', $oldProduct->id)->Update([
            'qty'=>$oldProduct->qty - $checkSell->qty,
        ]);
      }else{
      $checkNewProduct=Product::where('cate_id', $checkSell->cate_id)->where('brand_id', $checkSell->brand_id)->where('id', $checkSell->prod_id)->first();
      $updateNewProduct=Product::where('id', $checkNewProduct->id)->Update([
          'qty'=>$checkNewProduct->qty - $checkSell->qty,
      ]);
    }
    $checkCustomar=Customar::where('id', $checkSell->cus_id)->first();
    $updateCustomar=Customar::where('id', $checkSell->cus_id)->Update([
        'due'=>$checkCustomar->due + $checkSell->total_price,
    ]);
    Sale::where('id',$id)->update(['confirm'=>2]);
    session()->flash('message', 'Sell Confirm successfully.');
    return redirect()->back();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>
